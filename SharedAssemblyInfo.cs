﻿// <auto-generated />
using System.Reflection;

[assembly: AssemblyProduct("Pegasus")]
[assembly: AssemblyCopyright("Copyright © 2014 John Gietzen")]
[assembly: AssemblyVersion("3.2.0.0")]
[assembly: AssemblyFileVersion("3.2.0.0")]
#if !PORTABLE
[assembly: System.Runtime.InteropServices.ComVisible(false)]
#endif
